package br.com.vistoriafacil.domain.cidade;

import br.com.vistoriafacil.domain.imobiliaria.Imobiliaria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * Created by rafael on 08/05/15.
 */
@Repository
public interface CidadeRepository extends JpaRepository<Cidade, Long>, QueryDslPredicateExecutor<Cidade> {
}
