package br.com.vistoriafacil.security;



import org.springframework.boot.autoconfigure.jersey.JerseyProperties;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.annotation.Annotation;

/**
 * Created by rafael on 5/1/15.
 */
@Component
public class SimpleCORSFilter implements Filter
{

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        response.setHeader("Access-Control-Allow-Origin", "*");
        //response.setHeader("Access-Control-Allow-Origin", "https://vistoria-facil-app.herokuapp.com");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader( "Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Accept");
        filterChain.doFilter(servletRequest, response);

    }

    @Override
    public void destroy() {

    }
}
