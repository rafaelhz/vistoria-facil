package br.com.vistoriafacil.domain.acesso;

import javax.persistence.Id;
import java.util.Date;

/**
 * Created by rafael on 12/05/15.
 */

public class Acesso {

    @Id
    private String id;

    private Date data;

    private String usuario;

    public Acesso(Date data, String usuario) {
        this.data = data;
        this.usuario = usuario;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
