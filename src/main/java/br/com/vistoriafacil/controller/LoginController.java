package br.com.vistoriafacil.controller;

import br.com.vistoriafacil.domain.usuario.Usuario;
import br.com.vistoriafacil.domain.usuario.UsuarioService;
import br.com.vistoriafacil.domain.usuario.UsuarioTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by rafael on 4/25/15.
 */
@RestController("ws/login")
public class LoginController {

    @Autowired
    private UsuarioService usuarioService;

    @RequestMapping(value = "ws/login", method = RequestMethod.POST)
    public UsuarioTransfer login(@RequestBody String login) {
        Usuario usuario = usuarioService.findByLogin(login);
        if(usuario != null) {
            return new UsuarioTransfer(usuario);
        }
        return null;
    }

}
