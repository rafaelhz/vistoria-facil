package br.com.vistoriafacil.domain.usuario;

import java.util.List;

/**
 * Created by rafael on 5/2/15.
 */
public class UsuarioTransfer {

    private String nome;
    private String login;
    private List<Permissao> permissoes;

    public UsuarioTransfer() {
    }

    public UsuarioTransfer(Usuario usuario) {
        this.nome = usuario.getNome();
        this.login = usuario.getLogin();
        this.permissoes = usuario.getPermissoes();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public List<Permissao> getPermissoes() {
        return permissoes;
    }

    public void setPermissoes(List<Permissao> permissoes) {
        this.permissoes = permissoes;
    }
}
