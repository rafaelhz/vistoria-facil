package br.com.vistoriafacil.controller;

import br.com.vistoriafacil.domain.imobiliaria.Imobiliaria;
import br.com.vistoriafacil.domain.imobiliaria.ImobiliariaService;
import br.com.vistoriafacil.domain.imobiliaria.QImobiliaria;
import br.com.vistoriafacil.domain.usuario.Usuario;
import br.com.vistoriafacil.security.SecurityUser;
import com.mysema.query.BooleanBuilder;
import com.mysema.query.types.expr.BooleanExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.List;

/**
 * Created by rafael on 5/1/15.
 */
@RestController("ws/imobiliaria")
public class ImobiliariaController {

    @Autowired
    private ImobiliariaService imobiliariaService;

    @RequestMapping(value = "ws/imobiliaria", method = RequestMethod.GET)
    public Page<Imobiliaria> getTodos(@RequestParam(value="page", defaultValue = "0") Integer page,
                                      @RequestParam(value="limit", defaultValue = "3") Integer limit,
                                      @RequestParam(value="orderBy", defaultValue = "id") String orderBy,
                                      @RequestParam(value="orderDirection", defaultValue = "ASC") String orderDirection,
                                      @RequestParam(value="nome", defaultValue = "") String nome) {

        QImobiliaria imobiliaria = QImobiliaria.imobiliaria;
        BooleanBuilder where = new BooleanBuilder();

        if(nome != null && nome.length() > 0) {
            where.and(imobiliaria.nome.toUpperCase().like("%" + nome.toUpperCase() + "%"));
        }

        return imobiliariaService
                .getRepository()
                .findAll(where,
                        new PageRequest(page, limit, Sort.Direction.fromString(orderDirection), orderBy));
    }

    @RequestMapping(value = "ws/imobiliaria/{id}", method = RequestMethod.GET)
    public Imobiliaria get(@PathVariable("id") Long id) {
        return imobiliariaService.findOne(id);
    }

    @RequestMapping(value = "ws/imobiliaria", method = RequestMethod.POST)
    public Imobiliaria save(@RequestBody Imobiliaria imobiliaria) {
        if(imobiliaria.getId() == 0) {
            imobiliaria.setDataCadastro(Calendar.getInstance().getTime());
            imobiliaria.setUsuarioCadastro(getCurrentUser());
        }

        return imobiliariaService.save(imobiliaria);
    }

    @RequestMapping(value = "ws/imobiliaria/{id}", method = RequestMethod.DELETE)
    public void inativar(@PathVariable("id") Long id) {
        Imobiliaria imobiliaria = imobiliariaService.findOne(id);
        imobiliaria.setAtivo(false);
        imobiliariaService.save(imobiliaria);
    }

    private static Usuario getCurrentUser() {
        SecurityUser securityUser = (SecurityUser) SecurityContextHolder
                .getContext().getAuthentication().getPrincipal();

        return securityUser.getUsuario();
    }
}
