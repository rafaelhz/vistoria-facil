package br.com.vistoriafacil.domain.usuario;

import br.com.vistoriafacil.domain.imobiliaria.Imobiliaria;
import br.com.vistoriafacil.domain.usuario.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by rafael on 4/25/15.
 */
public interface UsuarioRepository extends JpaRepository<Usuario, Long>, QueryDslPredicateExecutor<Usuario> {

    @Query("select u from Usuario u LEFT JOIN FETCH u.permissoes where u.login = :login")
    public List<Usuario> findByLogin(@Param("login") String login);


}
