package br.com.vistoriafacil.domain.cidade;

import br.com.vistoriafacil.domain.imobiliaria.ImobiliariaRepository;
import br.com.vistoriafacil.infra.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * Created by rafael on 08/05/15.
 */
@Service
public class CidadeService extends AbstractService {

    @Autowired
    private CidadeRepository cidadeRepository;

    @Override
    public CidadeRepository getRepository() {
        return cidadeRepository;
    }
}
