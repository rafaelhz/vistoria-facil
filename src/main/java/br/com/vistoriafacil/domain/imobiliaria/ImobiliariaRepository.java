package br.com.vistoriafacil.domain.imobiliaria;

import com.mysema.query.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * Created by rafael on 5/1/15.
 */
@Repository
public interface ImobiliariaRepository extends JpaRepository<Imobiliaria, Long>, QueryDslPredicateExecutor<Imobiliaria> {


    public Page<Imobiliaria> findByNome(String nome, Pageable pageable);

}
