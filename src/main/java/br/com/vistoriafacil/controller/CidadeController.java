package br.com.vistoriafacil.controller;

import br.com.vistoriafacil.domain.cidade.Cidade;
import br.com.vistoriafacil.domain.cidade.CidadeService;
import br.com.vistoriafacil.domain.usuario.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by rafael on 08/05/15.
 */
@RestController("/cidade")
public class CidadeController {

    @Autowired
    private CidadeService cidadeService;

    @RequestMapping(value = "cidade", method = RequestMethod.GET)
    public List<Cidade> getTodas() {
        return cidadeService.getRepository().findAll();
    }
}
