package br.com.vistoriafacil.infra;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rafael on 29/04/15.
 */
public abstract class AbstractService {

    public abstract JpaRepository getRepository();

    public <T> T save(T entity) {
        return (T) getRepository().save(entity);
    }

    public List findAll() {
        return getRepository().findAll();
    }

    public <T> T findOne(Serializable id) {
        return (T) getRepository().findOne(id);
    }

    public void delete(Serializable id) {
        getRepository().delete(id);
    }
}
