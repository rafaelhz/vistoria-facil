package br.com.vistoriafacil.security;

import br.com.vistoriafacil.domain.usuario.Usuario;
import br.com.vistoriafacil.domain.usuario.UsuarioRepository;
import br.com.vistoriafacil.domain.usuario.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by rafael on 4/26/15.
 */
@Component
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UsuarioService usuarioService;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Usuario usuario = usuarioService.findByLogin(userName);

        if(usuario == null) {
            throw new UsernameNotFoundException("UserName " + userName + " not found");
        }
        return new SecurityUser(usuario);

    }
}
