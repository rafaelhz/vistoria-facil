package br.com.vistoriafacil.domain.acesso;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by rafael on 12/05/15.
 */
public interface AcessoRepository extends MongoRepository<Acesso, String> {


}
