package br.com.vistoriafacil.domain.usuario;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created by rafael on 5/2/15.
 */
public enum Permissao implements GrantedAuthority{

    USUARIO,
    ADMINISTRADOR,;

    @Override
    public String getAuthority() {
        return name();
    }
}
