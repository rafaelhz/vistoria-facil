package br.com.vistoriafacil.domain.vistoria;

import br.com.vistoriafacil.domain.cidade.Cidade;
import br.com.vistoriafacil.domain.imobiliaria.Imobiliaria;
import br.com.vistoriafacil.domain.modelo.Modelo;
import br.com.vistoriafacil.domain.usuario.Usuario;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by rafael on 5/10/15.
 */
@Entity
public class Vistoria implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_modelo", updatable = false)
    private Modelo modelo;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_imobiliaria", updatable = false)
    private Imobiliaria imobiliaria;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cidade", updatable = false)
    private Cidade cidade;

    private String locador;

    private String locataria;

    private String endereco;

    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_cadastro", updatable = false)
    private Usuario usuarioCadastro;

    @JsonIgnore
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "data_cadastro", updatable = false)
    private Date dataCadastro;
}
