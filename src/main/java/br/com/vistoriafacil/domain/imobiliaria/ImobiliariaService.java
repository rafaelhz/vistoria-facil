package br.com.vistoriafacil.domain.imobiliaria;

import br.com.vistoriafacil.infra.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 * Created by rafael on 5/1/15.
 */
@Service
public class ImobiliariaService extends AbstractService {

    @Autowired
    private ImobiliariaRepository imobiliariaRepository;

    @Override
    public ImobiliariaRepository getRepository() {
        return imobiliariaRepository;
    }
}
