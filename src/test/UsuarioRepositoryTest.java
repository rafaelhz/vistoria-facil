import br.com.vistoriafacil.Application;
import br.com.vistoriafacil.domain.usuario.Usuario;
import br.com.vistoriafacil.domain.usuario.UsuarioRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by rafael on 04/05/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
public class UsuarioRepositoryTest {

    @Autowired
    UsuarioRepository usuarioRepository;


    @Before
    public void initTest() {
        Usuario usuario = new Usuario();
        usuario.setAtivo(true);
        usuario.setLogin("manoel");
        usuario.setSenha("senha");
        usuarioRepository.save(usuario);
    }

    @Test
    public void deveEncontrarOUsuarioPorLogin() {

        List<Usuario> usuarios = usuarioRepository.findByLogin("manoel");
        Assert.assertFalse(usuarios.isEmpty());
    }


}
