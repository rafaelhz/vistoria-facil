package br.com.vistoriafacil.controller;

import br.com.vistoriafacil.domain.usuario.Usuario;
import br.com.vistoriafacil.domain.usuario.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by rafael on 4/25/15.
 */
@RestController("ws/admin")
public class AdminController {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @RequestMapping(value = "/ws/admin/todos", method = RequestMethod.GET)
    public List<Usuario> getTodos() {
        return usuarioRepository.findAll();
    }


}
