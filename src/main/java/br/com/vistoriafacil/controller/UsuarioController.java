package br.com.vistoriafacil.controller;

import br.com.vistoriafacil.domain.imobiliaria.QImobiliaria;
import br.com.vistoriafacil.domain.usuario.QUsuario;
import br.com.vistoriafacil.domain.usuario.Usuario;
import br.com.vistoriafacil.domain.usuario.UsuarioRepository;
import br.com.vistoriafacil.domain.usuario.UsuarioService;
import br.com.vistoriafacil.security.SecurityUser;
import com.mysema.query.BooleanBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Calendar;
import java.util.List;

/**
 * Created by rafael on 4/25/15.
 */
@RestController("/ws/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping(value = "/ws/usuario", method = RequestMethod.GET)
    public Page<Usuario> getTodos(@RequestParam(value="page", defaultValue = "0") Integer page,
                                      @RequestParam(value="limit", defaultValue = "3") Integer limit,
                                      @RequestParam(value="orderBy", defaultValue = "id") String orderBy,
                                      @RequestParam(value="orderDirection", defaultValue = "ASC") String orderDirection,
                                      @RequestParam(value="nome", defaultValue = "") String nome) {
        QUsuario usuario = QUsuario.usuario;
        BooleanBuilder where = new BooleanBuilder();

        if(nome != null && nome.length() > 0) {
            where.and(usuario.nome.toUpperCase().like("%" + nome.toUpperCase() + "%"));
        }

        return usuarioService
                .getRepository()
                .findAll(where,
                        new PageRequest(page, limit, Sort.Direction.fromString(orderDirection), orderBy));
    }

    @RequestMapping(value = "/ws/usuario/{id}", method = RequestMethod.GET)
    public Usuario get(@PathVariable("id") Long id) {
        return usuarioService.findOne(id);
    }

    @RequestMapping(value = "/ws/usuario", method = RequestMethod.POST)
    public Usuario save(@RequestBody Usuario usuario) {
        if(usuario.getId() == 0) {
            usuario.setAtivo(true);
            usuario.setSenha(passwordEncoder.encode(usuario.getSenha()));
        }
        return usuarioService.save(usuario);
    }

    @RequestMapping(value = "/ws/usuario/{id}", method = RequestMethod.DELETE)
    public void inativar(@PathVariable("id") Long id) {
        Usuario usuario = usuarioService.findOne(id);
        usuario.setAtivo(false);
        usuarioService.save(usuario);
    }
}
