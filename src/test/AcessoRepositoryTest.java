import br.com.vistoriafacil.Application;
import br.com.vistoriafacil.domain.acesso.Acesso;
import br.com.vistoriafacil.domain.acesso.AcessoRepository;
import org.aspectj.weaver.bcel.AtAjAttributes;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;

/**
 * Created by rafael on 12/05/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
public class AcessoRepositoryTest {

    @Autowired
    private AcessoRepository acessoRepository;

    @Before
    public void init() {
        acessoRepository.save(new Acesso(Calendar.getInstance().getTime(), "rafael"));
        acessoRepository.save(new Acesso(Calendar.getInstance().getTime(), "joao"));
    }

    @Test
    public void deveListarOsAcessos() {
        List acessos = acessoRepository.findAll();
        System.out.println(acessos.size());
        Assert.assertFalse(acessos.isEmpty());
    }
}
