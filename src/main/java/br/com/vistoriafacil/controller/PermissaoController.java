package br.com.vistoriafacil.controller;

import br.com.vistoriafacil.domain.usuario.Permissao;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rafael on 5/10/15.
 */
@RestController("/ws/permissao")
public class PermissaoController {

    @RequestMapping(value = "/ws/permissao", method = RequestMethod.GET)
    public List<Permissao> getTodas() {
        return Arrays.asList(Permissao.values());
    }


}
