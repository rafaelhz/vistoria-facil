-- CARGA INICIAL DO SISTEMA
INSERT INTO usuario (ativo, login, nome, senha) VALUES (true, 'rafael', 'rafael', '$2a$11$7EgaSkLIFAMpooeTmJNx7.QCXFfxyLHknN650gY1JZYKy3Ug5XCx2');
INSERT INTO usuario_permissao (usuario_id, permissao) VALUES (1, 'USUARIO');
INSERT INTO usuario_permissao (usuario_id, permissao) VALUES (1, 'ADMINISTRADOR');
INSERT INTO cidade (nome) VALUES ('LONDRINA - PR');
INSERT INTO cidade (nome) VALUES ('CAMBÉ - PR');
INSERT INTO cidade (nome) VALUES ('ROLANDIA - PR');
INSERT INTO cidade (nome) VALUES ('IBIPORÃ - PR');