package br.com.vistoriafacil.domain.usuario;

import br.com.vistoriafacil.infra.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by rafael on 29/04/15.
 */
@Service
public class UsuarioService extends AbstractService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public UsuarioRepository getRepository() {
        return usuarioRepository;
    }

    public Usuario findByLogin(String login) {
        List<Usuario> usuarios = usuarioRepository.findByLogin(login);
        if(!usuarios.isEmpty()) {
            return usuarios.get(0);
        }
        return null;
    }
}
